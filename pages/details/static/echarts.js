let radarChartData = {
	categories: ["汉口学院-电院楼", "汉口学院-电院楼", "汉口学院-电院楼", "汉口学院-行政楼", "汉口学院-图书馆", "汉口学院", "大花岭小区", "海天大厦"],
	series: [{
		name: "距离",
		data: [121, 121, 121, 121, 121, 121, 121, 121]
	}]
}
let radarChartOpts = {
	color: ["#1890FF", "#91CB74", "#FAC858", "#EE6666", "#73C0DE", "#3CA272", "#FC8452", "#9A60B4", "#ea7ccc"],
	padding: [5, 5, 5, 5],
	dataLabel: false,
	enableScroll: false,
	legend: {
		show: false,
		position: "right",
		lineHeight: 25
	},
	extra: {
		radar: {
			gridType: "circle",
			gridColor: "#CCCCCC",
			gridCount: 3,
			opacity: 0.2,
			max: 200,
			labelShow: true
		}
	}
}

export default {
	radarChartData,
	radarChartOpts
}
