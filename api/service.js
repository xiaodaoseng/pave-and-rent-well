import request from "@/utils/request.js";

// 添加信息
export function ServiceAdd(data) {
  return request.post("Service/Add", data);
}
export function ServiceUpdate(data) {
  return request.post("Service/update", data);
}

// 我的商家服务
export function OnMerchantApi(data) {
  return request.get("Service/OnMerchantList", data);
}

// 商家列表
export function getMerchantList(data) {
  return request.get("Service/List", data);
}

// 商家服务详情 - 附近推荐
export function NearbyMerchantApi(data) {
  return request.get("Service/NearbyMerchantList", data);
}
// 城市查询区域
export function getAreaApi(data) {
  return request.get("city/district", data);
}
//删除
export function deleteService(id) {
	return request.post(`Service/GroupDeletes/${id}`,{} , { noAuth : true });
}

