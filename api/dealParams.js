/**
 * 请求参数处理，筛选掉为null的属性，防止null作为字符串传入
 * （Object）params
 */
export const dealParams = (params)=>{
	if(!params)return;
	let dealData = {};
	for(let k in params){
		if(params[k] == null || params[k] == 'null')continue;
		dealData[k] = params[k]
	}
	return dealData;
}