// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import request from "@/utils/request.js";
/**
 * 首页接口，公共接口 ，优惠券接口 , 行业此讯 , 手机号码注册
 * 
 */

/**
 * 首页 第一级商品分类
 * 
 */
export function getCategoryFirst() {
	return request.get("product/category/get/first", {}, {
		noAuth: true
	});
}

/**
 * 首页 第三级商品分类
 * 
 */
export function getCategoryThird(id) {
	return request.get(`product/category/get/third/${id}`, {}, {
		noAuth: true
	});
}

/**
 * 首页 移动经营信息统计
 * 
 */
export function getCensusApi(data) {
	return request.get(`HomePage/HomeList`, data, {
		noAuth: true
	});
}

/**
 * 首页 获取城市区域tree结构的列表
 * 
 */
export function getCityApi(data) {
	return request.get(`city/list/tree`, data, {
		noAuth: true
	});
}

/**
 * @param {Object} data
 * 获取城市
 */

export function getlistasd(data) {
	return request.get(`city/listasd`, data, {
		noAuth: true
	});
}
export function getHotCity(data) {
	return request.get(`city/ConCityList`, data, {
		noAuth: true
	});
}
/**
 * 获取主页数据 无需授权
 * 
 */
export function getIndexData() {
	return request.get("index/info", {}, {
		noAuth: true
	});
}

/**
 * 获取主页数据 无需授权
 * 
 */
export function getBrandApi(data) {
	return request.get("index/BrandList", data, {
		noAuth: true
	});
}

/**
 * 获取主页数据 商家服务
 * 
 */
export function getTypeApi() {
	return request.get("Service/TypeList", {}, {
		noAuth: true
	});
}

/**
 * 获取主页数据 供应链
 * 
 */
export function getSupplyApi() {
	return request.get("product/SupplyList", {}, {
		noAuth: true
	});
}

/**
 * 获取主页数据 悬赏任务
 * 
 */
export function getRewardApi(data) {
	return request.get("Information/RewardList", data, {
		noAuth: true
	});
}

/**
 * 获取主页数据 
 * 
 */
export function getProjectApi(data) {
	return request.get("Project/list", data, {
		noAuth: true
	});
}

/**
 * 获取主页数据 经营信息分页列表
 * 
 */
export function getInformationApi(data) {
	return request.get("Information/list", data, {
		noAuth: true
	});
}
/**
 * 获取主页数据 经营信息分页列表
 * 
 */
export function getAllInfomation(data) {
	return request.get("Information/PagePullList", data, {
		noAuth: true
	});
}

/**
 * 获取主页数据 经营信息分页列表
 * 
 */
export function getInformationMapApi(data) {
	return request.get("Information/MapList", data, {
		noAuth: true
	});
}

/**
 * 获取主页数据 无需授权
 * 
 */
export function getStatisticsApi() {
	return request.get("index/statistics", {}, {
		noAuth: true
	});
}
/**
 * 获取主页数据 无需授权
 * 
 */
export function getStatisticsListApi(city) {
	return request.get(`ProjectHand/list/${city}`, {}, {
		noAuth: true
	});
}
/**
 * 获取登录授权login
 * 
 */
export function getLogo() {
	return request.get('wechat/getLogo', {}, {
		noAuth: true
	});
}


/**
 * 保存form_id
 * @param string formId 
 */
export function setFormId(formId) {
	return request.post("wechat/set_form_id", {
		formId: formId
	});
}

/**
 * 领取优惠卷
 * @param int couponId
 * 
 */
export function setCouponReceive(couponId) {
	return request.post(`coupon/receive/${couponId}`);
}
/**
 * 优惠券列表
 * @param object data
 */
export function getCoupons(data) {
	return request.get('coupon/page/list', data, {
		noAuth: true
	})
}

/**
 * 我的优惠券
 * @param int types 0全部  1未使用 2已使用
 */
export function getUserCoupons(data) {
	return request.get('coupon/user/list', data)
}

/**
 * 文章分类列表
 * 
 */
export function getArticleCategoryList() {
	return request.get('article/category/list', {}, {
		noAuth: true
	})
}

/**
 * 文章列表
 * @param int cid
 * 
 */
export function getArticleList(cid, data) {
	return request.get(`article/list/${cid}`, data, {
		noAuth: true
	})
}

/**
 * 文章 热门列表
 * 
 */
export function getArticleHotList() {
	return request.get('article/hot/list', {}, {
		noAuth: true
	});
}

/**
 * 文章 轮播列表
 * 
 */
export function getArticleBannerList() {
	return request.get('article/banner/list', {}, {
		noAuth: true
	})
}

/**
 * 文章详情
 * @param int id 
 * 
 */
export function getArticleDetails(id) {
	return request.get(`article/info/${id}`, {
		noAuth: true
	});
}

/**
 * 手机号+验证码登录接口
 * @param object data
 */
// export function loginMobile(data){
//   return request.post('login/mobile',data,{noAuth:true})
// }

/**
 * 获取短信KEY
 * @param object phone
 */
export function verifyCode() {
	return request.get('verify_code', {}, {
		noAuth: true
	})
}

/**
 * 获取用户手机号验证码发送
 * @param object phone
 */
export function registerVerify(phone) {
	return request.post('user/phone/code', {
		noAuth: true
	})
}

/**
 * 换绑手机号获取验证码
 * @param object data
 */
export function bindingPhoneCode(data) {
	return request.post('user/update/binding/phone/code', data, {
		noAuth: true
	})
}

/**
 * 手机号注册
 * @param object data
 * 
 */
export function phoneRegister(data) {
	return request.post('register', data, {
		noAuth: true
	});
}

/**
 * 手机号修改密码
 * @param object data
 * 
 */
export function phoneRegisterReset(data) {
	return request.post('user/register/reset', data, {
		noAuth: true
	})
}

/**
 * 手机号+密码登录
 * @param object data
 * 
 */
export function phoneLogin(data) {
	return request.post('login', data, {
		noAuth: true
	})
}

/**
 * 切换H5登录
 * @param object data
 */
// #ifdef MP
export function switchH5Login() {
	return request.post('switch_h5', {
		'from': 'routine'
	});
}
// #endif

/*
 * h5切换公众号登录
 * */
// #ifdef H5
export function switchH5Login() {
	return request.post("switch_h5", {
		'from': "wechat"
	});
}
// #endif

/**
 * 换绑手机号
 * 
 */
export function bindingPhone(data) {
	return request.post('user/update/binding', data);
}

/**
 * 换绑手机号校验
 * 
 */
export function bindingVerify(data) {
	return request.post('update/binding/verify', data);
}

/**
 * 获取订阅消息id
 */
export function getTemlIds(data) {
	return request.get('wechat/program/my/temp/list', data, {
		noAuth: true
	});
}

/**
 * 首页拼团数据
 */
export function pink() {
	return request.get('pink', {}, {
		noAuth: true
	});
}

/**
 * 获取城市信息
 */
export function getCity(data) {
	return request.get('city/list', data, {
		noAuth: true
	});
}

/**
 * 获取小程序直播列表
 */
export function getLiveList(page, limit) {
	return request.get('wechat/live', {
		page,
		limit
	}, {
		noAuth: true
	});
}

/**
 * 获取小程序二维码
 */
export function mpQrcode(data) {
	return request.post('qrcode/get/wechat', data, {
		noAuth: true
	});
}

/**
 * 获取主题换色配置
 */
export function getTheme() {
	return request.get('index/color/config', {}, {
		noAuth: true
	});
}

/**
 * 获取主题换色配置
 */
export function getAppVersion() {
	return request.get('index/get/version', {}, {
		noAuth: true
	});
}

/**
 * 获取全局本地图片域名
 */
export function getImageDomain() {
	return request.get('image/domain', {}, {
		noAuth: true
	});
}

/**
 * 商品排行榜
 */
export function productRank() {
	return request.get('product/leaderboard', {}, {
		noAuth: true
	});
}

/**
 * 协议详情
 */
export function agreementInfo(info) {
	return request.get(`agreement/${info}`, {}, {
		noAuth: true
	});
}

/**
 * 地图找铺 
 */
export function getTudListApi(data) {
	return request.get(`Information/tudList`, data, {
		noAuth: true
	});
}

/**
 * 我的二手闲置
 */
export function getSecondhandListApi(data) {
	return request.get(`Secondhand/SecondhandList`, data, {
		noAuth: true
	});
}

/**
 * 二手买卖
 */
export function getSellListApi(data) {
	return request.get(`Secondhand/SellList`, data, {
		noAuth: true
	});
}

/**
 * 商品
 */
export function getMerchantListApi(data) {
	return request.get(`Service/MerchantList`, data, {
		noAuth: true
	});
}

/**
 * 商品详情
 */
export function getMerchantDetailApi(id, data) {
	return request.get(`Service/get/MerchantList/${id}`, data, {
		noAuth: true
	});
}
/**
 * 我的供应链
 */
export function getOnSupplyListApi(data) {
	return request.get(`Secondhand/OnSupplyList`, data, {
		noAuth: true
	});
}

/**
 * RewardUser/add
 */
export function addReawardUser(data) {
	return request.post(`RewardUser/add`, data, {
		noAuth: true
	});
}

/**
 * 加入服务商 
 * Service/ServiceAdd
 */
export function addService(data) {
	return request.post(`Service/ServiceAdd`, data, {
		noAuth: true
	});
}
/**
 * 
 */
export function getOnMerchantListApi(data) {
	return request.get(`Service/OnMerchantList`, data, {
		noAuth: true
	});
}

/**
 * 首页地图找铺统计
 */
export function getHomePageMapList(data) {
	return request.get(`HomePage/MapList`, data, {
		noAuth: true
	});
}

/**
 个人中心下一栏统计
*/
export function getIndividual(data) {
	return request.get(`HomePage/CenterList`, data, {
		noAuth: true
	});
}

/**
 个人中心最下面精选推荐
*/
export function getRecommend(data) {
	return request.get(`Information/RecommendList`, data, {
		noAuth: true
	});
}

/**
 * 是否已关注
 */
export function getWhether(data) {
	return request.get(`UserFriend/whether`, data, {
		noAuth: true
	});
}
/**
 * 添加收藏
 */
export function addCollect(data) {
	return request.post(`UserCollect/add`, data, {
		noAuth: true
	});
}
/**
 * 取消收藏
 */
export function delCollect(data) {
	return request.get(`UserCollect/delete`, data, {
		noAuth: true
	});
}
/**
 * 是否已收藏
 */
export function existsCollect(data) {
	return request.get(`UserCollect/ByList`, data, {
		noAuth: true
	});
}
/**
 * 项目精选推荐
 */
export function getProject(data) {
	return request.get(`Project/RecommendList`, data, {
		noAuth: true
	});
}
/**
 * 商家服务精选推荐
 */
export function getService(data) {
	return request.get(`Service/RecommendList`, data, {
		noAuth: true
	});
}
/**
 * 商家服务最新查询
 */
export function getServiceList(data) {
	return request.get(`Service/MerchantList`, data, {
		noAuth: true
	});
}

/**
 * 添加供应链 
 * Service/Service
 */
export function addSupple(data) {
	return request.post(`Supple/add`, data, {
		noAuth: true
	});
}
/**
 *获取供应链接口Supple/get/list/{id}
 */
export function getSuppleList(data) {
	return request.get(`Supple/list`, data, {
		noAuth: true
	});
}
/**
 *获取供应链接口Supple/get/list/{id}
 */
export function getSuppleOnList(data) {
	return request.get(`Supple/OnList`, data, {
		noAuth: true
	});
}
/**
 * 添加求购供应链 
 * Service/Service
 */
export function addSuppleBeg(data) {
	return request.post(`SuppleBeg/add`, data, {
		noAuth: true
	});
}
/**
 * 更新求购供应链 
 * Service/Service
 */
export function editSuppleBeg(data) {
	return request.post(`SuppleBeg/update`, data, {
		noAuth: true
	});
}
/**
 *获取供应链接口Supple/get/list/{id}
 */
export function getSuppleBegList(data) {
	return request.get(`SuppleBeg/list`, data, {
		noAuth: true
	});
}
/**
 *获取我的供应链接口Supple/get/list/{id}
 */
export function getSuppleBegOnList(data) {
	return request.get(`SuppleBeg/OnList`, data, {
		noAuth: true
	});
}
/**
 *求购供应链详情 
 */
export function getSuppleBegDetail(id, data) {
	return request.get(`SuppleBeg/get/list/${id}`, data ? data : {}, {
		noAuth: true
	});
}
/**
 *供应链详情 
 */
export function getSuppleDetail(id, data) {
	return request.get(`Supple/get/list/${id}`, data ? data : {}, {
		noAuth: true
	});
}
/**
 * 更新供应链 
 * Service/Service
 */
export function editSupple(data) {
	return request.post(`Supple/update`, data, {
		noAuth: true
	});
}

/**
 * 供应链删除
 */
export function deleteSupple(id) {
	return request.post(`Supple/delete/${id}`, {}, {
		noAuth: true
	});
}
/**
 * 求购供应链删除
 */
export function deleteSuppleBeg(id) {
	return request.post(`SuppleBeg/delete/${id}`, {}, {
		noAuth: true
	});
}
/**
 * 悬赏提示金币规则
 */
export function getCoinList(data) {
	return request.get(`Reward/coin`, data, {
		noAuth: true
	});
}



/**
 * 金币与人民币兑换
 */
export function getMonList(data) {
	return request.get(`Reward/CashList`, data, {
		noAuth: true
	});
}
/**
 * 供应链最新分页查询
 */
export function getNewList(data) {
	return request.get(`Supple/NewList`, data, {
		noAuth: true
	});
}
/**
 * 供应链精选推荐分页查询
 */
export function getRecommendList(data) {
	return request.get(`Supple/Recommend`, data, {
		noAuth: true
	});
}
/**
 * 详情获取周边api/front/Information/Peripheral 
 */
export function getZb(data) {
	return request.get(`Information/Peripheral`, data, {
		noAuth: true
	});
}
/**
 * 信息纠错
 */
export function xxjc(data) {
	return request.post(`Feedback/add`, data, {
		noAuth: true
	});
}
/**
 * 获取详情对手api/front/HomePage/Peripheral  
 */
export function HomePagePeripheral(data) {
	return request.get(`HomePage/Peripheral`, data, {
		noAuth: true
	});
}
/**
 * 为你推荐
 */
export function RecommendToYou(data) {
	return request.post(`DateMatch/TotalMatching`, data, {
		noAuth: true
	});
}
/**
 * 获取商圈/Business/list
 */
export function BusinessList(data) {
	return request.get(`Business/list`, data, {
		noAuth: true
	});
}
/**
 * 置顶
 * jy 经营信息
 * qz 求组
 * qg 求购
 */
export function zdInformation(type, data) {
	let url = {
		'jy': 'Information/InfoPinned',
		'qz': 'Information/PhzaskPinned',
		'qg': 'Information/BuyPinned',
		'recruiting': 'Recruit/RecruitingPinned',
		'job': 'Recruit/JobPinned',
		'supply':'Supple/supplyPinned',
		'supplyBeg':'SuppleBeg/supplyPinned',
		'unused':'ProjectHand/supplyPinned',
		'sj': 'Service/InfoPinned'
	}
	return request.post(url[type], data, {
		noAuth: true
	});

}
/**
 * 立即托管 Reward/trusteeship
 */
export function reardTrusteeship(data) {
	return request.post("Reward/trusteeship", data, {
		noAuth: true
	});

}
/**
 *  jingying设置
 */
export function jySettingTo(data) {
	return request.post("Information/update", data, {
		noAuth: true
	});

}
/**
 *  jingying设置
 */
export function qzSettingTo(data) {
	return request.post("Information/PhzaskUpdate", data, {
		noAuth: true
	});

}
/**
 * Business/selectList
 */
export function selectListBusiness() {
	return request.get(`Business/selectList`, {}, {
		noAuth: true
	});
}
/**
 *  jingying设置api/front/Information/updateDelete/{id}
 */
export function qgSettingTo(data) {
	return request.post("Information/BuyUpdate", data, {
		noAuth: true
	});

}
/**
 * @param {Object} data
 */
export function jyDel(id,storeCategory,status) {
	return request.post(`Information/updateDelete/${id}?storeCategory=`+(storeCategory?storeCategory:'')+'&status='+(status?status:''), {}, {
		noAuth: true
	});
}
/**
 * @param {Object} data
 */
export function qzDel(id,storeCategory,status) {
	return request.post(`Information/GroupDeletes/${id}?storeCategory=`+(storeCategory?storeCategory:'')+'&status='+(status?status:''), {}, {
		noAuth: true
	});
}
/**
 * @param {Object} data
 */
export function qgDel(id,storeCategory,status) {
	return request.post(`Information/deletes/${id}?storeCategory=`+(storeCategory?storeCategory:'')+'&status='+(status?status:''), {}, {
		noAuth: true
	});
}
/**
 * 拨打电话 index/phoneCoin
 */
export function tophoneCoin(params) {
	return request.get(`index/phoneCoin`, params, {
		noAuth: true
	});
}
/**
 * 访客列表 UserVisitRecord/Visitor
 */
export function visitorList(data) {
	return request.get(`UserVisitRecord/Visitor`, data, {
		noAuth: true
		
	});
}
/**
 * Statement/OnList 
 */
export function StatementOnList(data) {
	return request.get(`Statement/OnList`, data, {
		noAuth: true
	});
}
/**
 * @param {Object} data
 */
export function StatementAdd(data) {
	return request.post(`Statement/add `, data, {
		noAuth: true
	});
}
/**
 * Statement/Delete
 */
export function StatementDelete(id) {
	return request.post(`Statement/Delete/${id}`, {}, {
		noAuth: true
	});
}
/**
 * Business/MapList
 * 
 */
export function BusinessMapList(data) {
	return request.get(`Business/MapList`, data, {
		noAuth: true
	});
}

/**
 * /Reward/CoinL
 */
export function RewardCoinL() {
	return request.get(`Reward/CoinL`, {}, {
		noAuth: true
	});
}

/**
 * 公司 分页列表
 * 
 */
export function getCompanyApi(data) {
	return request.get("Company/list", data, {
		noAuth: true
	});
}
/**
 * 公司 分页列表
 * 
 */
export function getLocationApi(data) {
	return new Promise((reslove,reject)=>{
		uni.request({
			url: "https://apis.map.qq.com/ws/geocoder/v1/",
			method: 'GET',
			data,
			success: (res) => {
				reslove(res.data)
			},
			fail: (msg) => {
				reject('请求失败');
			}
			
		})
	})
}
/**
 * 小程序判断用户求租、求购信息经纬度，用户禁用激活状态
 * 
 */
export function getApplet(data) {
	return request.get("city/longitude", data, {
		noAuth: true
	});
}


export function getKftoPhone(data) {
	return request.get("city/ConList", data, {
		noAuth: true
	});
}

export function getKeywords(data) {
	return request.get("UserKeyword/list", data, {
		noAuth: true
	});
}
// 搜索关键字
export function keywordList(data){
	return request.get("UserKeyword/keywordList", data, {
		noAuth: true
	});
}

// 悬赏
export function closeReward(data){
	return request.post("Reward/Off", data, {
		noAuth: true
	});
}
// 悬赏
export function deleteReward(ids){
	return request.post(`Reward/deletes/${ids}`, {}, {
		noAuth: true
	});
}

// 获取默认定位城市
export function getDefaultLoacation(){
	return request.get(`user/getCity`, {}, {
		noAuth: true
	});
}
/**
 * 二手商品
 * 
 */
export function getProduct(data) {
	return request.get(`product/onProduct`, data, {
		noAuth: true
	});
}