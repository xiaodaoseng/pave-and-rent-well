
import request from "@/utils/request.js";

/**
 * 会员类别
 * @param 
 */
export function getVipList(data) {
	return request.get("Vip/vipList",data , { noAuth : true });
}
/**
 * 会员套餐
 * @param  vipId
 */
export function getvipPackageList(data) {
	return request.get("Vip/vipPackageList",data , { noAuth : true });
}
/**
 * 开通接口
 */
export function toKtVip(data) {
	 return  request.post("Vip/add",data);
}