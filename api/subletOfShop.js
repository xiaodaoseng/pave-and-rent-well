import request from "@/utils/request.js";

// 添加出售，出租，转让信息
export function informationAdd(data) {
  return request.post("Information/add", data);
}

// 字典
export function dictionary(data) {
  return request.get("index/get/general/dictionary", data);
}

// 经营类型
export function classificationList(data) {
  return request.get("Information/classificationList", data);
}

//添加联系人
export function contactAdd(data) {
	return request.post("Contact/add", data);
}

//编辑联系人
export function contactUpdate(data) {
	return request.post("Contact/update", data);
}

//编辑联系人
export function contactDelete(id) {
	return request.post(`Contact/deletes/${id}`, {});
}

//求租信息分页列表
export function getGroupListApi(data) {
	return request.get("Information/GroupList", data,{ noAuth : true });
}

//求租地图找铺信息分页列表
export function getMapGroupListApi(data) {
	return request.get("Information/MapGroupList", data);
}

//我的求租信息分页列表
export function getOnGroupListApi(data) {
	return request.get("Information/OnGroupList", data,{ noAuth : true });
}

// 添加求租
export function InformationGroupAdd(data) {
	return request.post("Information/GroupAdd", data);
}

// 添加求购
export function PurchaseAdd(data) {
	return request.post("Information/PurchaseAdd", data);
}

// 查询品牌信息
export function BrandList(data) {
  return request.get("Brand/list", data);
}

//查询所属项目
// front/Project/list
export function ProjectList(data) {
  return request.get("Project/list", data);
}
//查询经营户
// front/Project/list
export function StoreList(data) {
  return request.get("Recruit/infoList", data);
}
//获取我的商铺发布所有列表
export const AllListApi = (data) => {
	return request.get('Information/AllList', data, { noAuth : true })
}

//获取我的商铺发布出，转，售
export const OnListApi = (data) => {
	return request.get('Information/OnList', data, { noAuth : true })
}

//获取我的商铺发布 求购列表
export const BuyListApi = (data) => {
	return request.get('Information/BuyList', data, { noAuth : true })
}
//获取我的商铺发布 求购列表
export const updateBuy = (data) => {
	return request.post('Information/BuyUpdate', data, { noAuth : true })
}
//获取我的商铺发布 求购列表
export const onBuyListApi = (data) => {
	return request.get('Information/OnBuyList', data, { noAuth : true })
}
//求购地图找铺列表
export const MapBuyApi = (data) => {
	return request.get('Information/MapBuyList', data, { noAuth : true })
}

//获取我的商铺发布出租详情
export const ShopRentDetailApi = (id,data) => {
	return request.get(`Information/get/list/${id}`, data, { noAuth : true })
}

//获取我的商铺发布出租详情
export const ShopRentDetailApiNoDw = (id,data) => {
	
	return request.get(`Information/get/lists/${id}`, data, { noAuth : true })
}
//附近经营信息分页列表
export const NearbyApi = (data) => {
	return request.get(`Information/NearbyList`, data, { noAuth : true })
}

//获取求租详情信息
export const GroupDetailApi = (id,data) => {
	return request.get(`Information/get/GroupList/${id}`, data, { noAuth : true })
}

//获取求购详情信息
export const BuyDetailApi = (id,data) => {
	return request.get(`Information/get/BuyList/${id}`, data, { noAuth : true })
}

//获取地图找铺列表
export const MapAllApi = (data) => {
	return request.get(`Information/MapAllList`, data, { noAuth : true })
}

/**
 * 继续完善 == 编辑 Information/update
 */
export const editInfo = data=>(request.post(`Information/update`, data, { noAuth : true }))
/**
 * 求租的更新
 */
export const editPhzasInfo = data=>(request.post(`Information/PhzaskUpdate`, data, { noAuth : true }))



//搜索信息
export const searchData = (data) => {
	return request.get(`HomePage/AllList`, data)
}

// 图片识别店铺入库
export const aiPhotoApi = (data) => {
	return request.post(`Information/get/hir `, data)
}