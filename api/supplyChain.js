import request from "@/utils/request.js";

// 供应链发布添加
export function SupplyAdd(data) {
  return request.post("product/SupplyAdd", data);
}

// 移动供应链
export function SecondhandAdd(data) {
  return request.get("Secondhand/SupplyList", data);
}

// 查询商品分类
export function SupplyList(data) {
  return request.get("product/SupplyList", data);
}

// 查询最新供应链列表
export function getSupplyLatestApi(data) {
  return request.get("Secondhand/SupplyLatestList", data);
}
