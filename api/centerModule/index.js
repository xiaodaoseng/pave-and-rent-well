import request from "@/utils/request.js";

//获取项目列表
export function getprojectListApi(data){
  return request.get("Project/list",data,{ noAuth : true});
}
//添加我的物业
export function addProperty(data){
	return request.post("Property/add",data);
}

//分页获取物业
export function getOnList(data){
	return request.get("Property/OnList",data);
}
//获取物业详情
export function getDetail(id){
	return request.get(`Property/get/list/${id}`);
}
//删除物业
export function delProperty(id){
	return request.post(`Property/delete/${id}`);
}
//修改物业
export function editProperty(data){
	return request.post(`Property/update`,data);
}


//生意查询 
export function getPointOnList(data){
	return request.get(`Point/OnList`,data,{ noAuth : true});
}
//获取我的布点详情
export function getPointDetail(id){
	return request.get(`Point/get/details/${id}`);
}
//编辑布点
export function editPoint(data){
	return request.post("Point/update",data);
}
//删除布点
export function delPonit(id){
	return request.post(`Point/deletes/${id}`);
}


//添加我的项目
export function addProject(data){
	return request.post("Project/add",data);
}
//获取我的项目详情
export function getProjectDetail(id){
	return request.get(`Project/get/list/${id}`);
}

//编辑
export function editProject(data){
	return request.post("Project/update",data);
}

//获取我的项目列表
export function getProjectList(data){
	return request.get('Project/OnList',data);
}

//删除物业
export function delProject(id){
	return request.post(`Project/delete/${id}`);
}

//获取所属行业
export function getBusiness(){
	return request.get('Recruit/Industrylist');
}



//添加我的企业
export function addEntert(data){
	return request.post("Company/add",data);
}
//编辑我的企业
export function editEnter(data){
	return request.post("Company/update",data);
}

//获取我的项目详情
export function getEnterDetail(id){
	return request.get(`Company/get/details/${id}`);
}

//获取我的企业列表
export function getCompanyList(data){
	return request.get('Company/OnList',data);
}

//删除企业
export function delCompany(id){
	return request.post(`Company/deletes/${id}`);
}
//删除联系人
export function delConcats(id){
	return request.post(`Contact/deletes/${id}`);
}