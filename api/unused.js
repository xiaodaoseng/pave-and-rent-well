
import request from "@/utils/request.js";

/**
 * 二手新增
 * @param 
 */
export function addUnused(data) {
	return request.post("ProjectHand/add",data , { noAuth : true });
}
//我的二手
export function unusedListOnline(data) {
	return request.get("ProjectHand/OnList",data , { noAuth : true });
}
//二手列表
export function unusedList(data) {
	return request.get("ProjectHand/list",data , { noAuth : true });
}
/**
 * 二手详情
 * @param 
 */
export function getUnusedDetail(id) {
	return request.get(`ProjectHand/get/list/${id}`,{},{ noAuth : true });
}
/**
 * 二手编辑
 * @param 
 */
export function editUnused(data) {
	return request.post("ProjectHand/update",data , { noAuth : true });
}
/**
 * 二手删除
 * @param 
 */
export function deleteUnused(id) {
	return request.post(`ProjectHand/updateDelete/${id}`,{} , { noAuth : true });
}