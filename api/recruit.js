
import request from "@/utils/request.js";
/**
 * 首页招工接口
 * 
*/

/**
 * 招工列表
 * 
*/
export function getRecruitList(data)
{
  return request.get("Recruit/list",data,{ noAuth : true });
}
/**
 * 求职列表
 * 
*/
export function getJobList(data)
{
  return request.get("Recruit/JobList",data,{ noAuth : true });
}
/**
 * 招工最新列表
 * 
*/
export function getRecruitLatesApi(data)
{
  return request.get("Recruit/latestList",data,{ noAuth : true });
}
/**
 * 新增招工 发布信息
 * 
*/
export function getRecruitAddApi(data)
{
  return request.post("Recruit/add",data,{ noAuth : true });
}

/**
 * 布点分页
 * 
*/
export function getPointListApi(data)
{
  return request.get("Point/list",data,{ noAuth : true });
}

/**
 * 行业分类列表
 * 
*/
export function getIndustryListApi(data)
{
  return request.get("Recruit/Industrylist",data,{ noAuth : true });
}

/**
 * 求职新增
 * 
*/
export function getJobAddApi(data)
{
  return request.post("Recruit/JobAdd",data,{ noAuth : true });
}

/**
 * 求职编辑
 * 
*/
export function getJobUpdateApi(data)
{
  return request.post("Recruit/JobUpdate",data,{ noAuth : true });
}

/**
 * 求职 期望职位
 * 
*/
export function getPositionListApi(data)
{
  return request.get("Recruit/PositionList",data,{ noAuth : true });
}

/**
 * 我的发布-招工 全部
 * 
*/
export function getObjectListApi(data)
{
  return request.get("Recruit/objectList",data,{ noAuth : true });
}
/**
 * 我的发布-求职 批量删除求职信息
 * 
*/
export function JobDeletesApi(id,data)
{
  return request.post(`Recruit/updateDelete/${id}`,data,{ noAuth : true });
}
/**
 * 我的发布-招工 求职
 * 
*/
export function getOnJobApi(data)
{
  return request.get("Recruit/OnJobList",data,{ noAuth : true });
}

/**
 * 我的发布-招工 招工
 * 
*/
export function getOnRecruitApi(data)
{
  return request.get("Recruit/OnList",data,{ noAuth : true });
}

/**
 * 我的发布-招工 招工详情
 * 
*/
export function getRecruitDetailApi(id,data)
{
  return request.get(`Recruit/get/list/${id}`,data,{ noAuth : true });
}

/**
 * 我的发布-求职 求职详情
 * 
*/
export function jobHuntingDetailApi(id,data)
{
  return request.get(`Recruit/get/JobList/${id}`,data,{ noAuth : true });
}

/**
 * 我的发布-招工 批量删除招工信息
 * 
*/
export function RecruitDeletesApi(id,data)
{
  return request.post(`Recruit/deletes/${id}`,data,{ noAuth : true });
}

/**
 * 我的发布-招工 编辑招工信息
 * 
*/
export function RecruitUpdateApi(data)
{
  return request.post(`Recruit/update`,data,{ noAuth : true });
}

/**
 * 我的发布-求职 求职详情
 * 
*/
export function getRecruitCategoryNum(data)
{
  return request.get(`Recruit/HomeSize`,data,{ noAuth : true });
}