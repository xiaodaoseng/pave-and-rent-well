
import request from "@/utils/request.js";

/**
 * 获取购物车数量
 * @param numType boolean true 购物车数量,false=购物车产品数量
 */
export function getPointListApi(data) {
	return request.get("Point/list",data , { noAuth : true });
}