
import request from "@/utils/request.js";

/**
 * 获取站内信
 * @param 
 */
export function unusedList() {
	return request.get("Inside/uid",{} , { noAuth : true });
}
/**
 * 站内信已读
 * @param 
 */
export function inside(data) {
	return request.post("Inside/update",data , { noAuth : true });
}