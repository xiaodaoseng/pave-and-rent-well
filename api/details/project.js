import request from "@/utils/request.js";
/**
 * 首页接口，公共接口 ，优惠券接口 , 行业此讯 , 手机号码注册
 * 
*/

/**
 * 详情 
 * 
*/
export function getProjectDetailApi(id,params)
{
  return request.get(`Project/get/list/${id}`,params,{ noAuth : true});
}
