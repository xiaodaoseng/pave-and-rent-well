
import request from "@/utils/request.js";
/**
 * 首页接口，公共接口 ，优惠券接口 , 行业此讯 , 手机号码注册
 * 
*/

/**
 * 发布品牌 所属类别
 * 
*/
export function getclassificationListApi()
{
  return request.get("Information/classificationList",{},{ noAuth : true});
}

/**
 * 发布品牌 
 * 
*/
export function getBrandAddApi(data)
{
  return request.post("Brand/add",data,{ noAuth : true});
}

/**
 * 品牌 分页列表
 * 
*/
export function getBrandListApi(data)
{
  return request.get("Brand/list",data,{ noAuth : true});
}

/**
 * 发布品牌 增加加盟申请
 * 
*/
export function getApplyAddApi(data)
{
  return request.post("Brand/ApplyAdd",data,{ noAuth : true});
}

/**
 * 发布品牌 移动端增加代理加盟
 * 
*/
export function getAgentAddApi(data)
{
  return request.post("Brand/AgentAdd",data,{ noAuth : true});
}

/**
 * 发布品牌 移动端增加布点信息
 * 
*/
export function getPointAddApi(data)
{
  return request.post("Point/add",data,{ noAuth : true});
}

/**
 * 发布品牌 品牌故事
 * 
*/
export function getBrandStoryAddApi(data)
{
  return request.post("Brand/StoryAdd",data,{ noAuth : true});
}

/**
 * 我的品牌 品牌列表
 * 
*/
export function getBrandOnListApi(data)
{
  return request.get("Brand/OnList",data,{ noAuth : true});
}

/**
 * 我的品牌 品牌详情
 * 
*/
export function getBrandDetailsApi(id,data)
{
  return request.get(`Brand/get/details/${id}`,data,{ noAuth : true});
}

/**
 * 我的品牌 加盟列表
 * 
*/
export function getAgentListApi(data)
{
  return request.get(`Brand/AgentList`,data,{ noAuth : true});
}

/**
 * 我的品牌 详情 品牌故事
 * 
*/
export function getStoryListApi(data)
{
  return request.get(`Brand/StoryList`,data,{ noAuth : true});
}

/**
 * 我的品牌 编辑
 * 
*/
export function updateBrandApi(data)
{
  return request.post(`Brand/update`,data,{ noAuth : true});
}
