import request from "@/utils/request.js";


/**
 * 数据匹配权重模板分页查询
 * 
*/
export function DateMatchListApi(data)
{
  return request.get("DateMatch/List",data,{ noAuth : true });
}

/**
 * 添加
 * 
*/
export function UserMatchAddApi(data)
{
  return request.post("UserMatch/add",data,{ noAuth : true });
}

/**
 * 查询用户匹配模板
 * 
*/
export function UserMatchlistApi(data)
{
  return request.get("UserMatch/list",data,{ noAuth : true });
}
/**
 * @UserMatch/delete/{id}
 */
export function DelUserMatch(id)
{
  return request.post(`UserMatch/delete/${id}`,{},{ noAuth : true });
}
/**
 * 查询用户匹配模板
 * 
*/
export function UserMatchUpdateApi(data)
{
  return request.post("UserMatch/update",data,{ noAuth : true });
}

/**
 * 求租数据匹配权重模板分页查询
 * 
*/
export function DateMatchInFoApi(data)
{
  return request.get("DateMatch/InFoList",data,{ noAuth : true });
}

/**
 * 求职数据匹配权重模板分页查询
 * 
*/
export function DateMatchJobApi(data)
{
  return request.get("DateMatch/JobList",data,{ noAuth : true });
}

/**
 * 求购数据匹配权重模板分页查询
 * 
*/
export function DateMatchBuyApi(data)
{
  return request.get("DateMatch/BuyList",data,{ noAuth : true });
}

/**
 * 招工数据匹配权重模板分页查询
 * 
*/
export function DateMatchRecruitApi(data)
{
  return request.get("DateMatch/RecruitList",data,{ noAuth : true });
}

/**
 * 出租数据匹配权重模板分页查询
 * 
*/
export function DateMatchLetListApi(data)
{
  return request.get("DateMatch/LetList",data,{ noAuth : true });
}

/**
 * 供应商家服务数据匹配权重模板分页查询
 * 
*/
export function DateMatchSupplyApi(data)
{
  return request.get("DateMatch/SupplyList",data,{ noAuth : true });
}

/**
 * 转让、出售数据匹配权重模板分页查询
 * 
*/
export function DateMatchTransferSaleApi(data)
{
  return request.get("DateMatch/TransferSaleList",data,{ noAuth : true });
}

/**
 * 找商家服务数据匹配权重模板分页查询
 * 
*/
export function DateMatchForServiceApi(data)
{
  return request.get("DateMatch/ForServiceList",data,{ noAuth : true });
}

/**
 * 找商家服务数据匹配权重模板分页查询
 * 
*/
export function DateMatchDeteleApi(id)
{
  return request.post(`UserMatch/delete/${id}`,{ },{ noAuth : true });
}

/**
 * 最新出租数据匹配权重模板分页查询
 * 
*/
export function DateMatchNewLetApi(data)
{
  return request.get('DateMatch/NewLetList',data,{ noAuth : true });
}

/**
 * 最新求购数据匹配权重模板分页查询
 * 
*/
export function DateMatchNewBuyApi(data)
{
  return request.get('DateMatch/NewBuyList',data,{ noAuth : true });
}

/**
 * 最新商家服务匹配权重模板分页查询
 * 
*/
export function DateMatchNewForServiceApi(data)
{
  return request.get('DateMatch/NewForServiceList',data,{ noAuth : true });
}

/**
 * 最新供应商家服务匹配权重模板分页查询
 * 
*/
export function DateMatchNewSupplyApi(data)
{
  return request.get('DateMatch/NewSupplyList',data,{ noAuth : true });
}

/**
 * 最新转让、出售匹配权重模板分页查询
 * 
*/
export function DateMatchNewTransferSaleApi(data)
{
  return request.get('DateMatch/NewTransferSaleList',data,{ noAuth : true });
}

/**
 * 最新招工匹配权重模板分页查询
 * 
*/
export function DateMatchNewRecruitApi(data)
{
  return request.get('DateMatch/NewRecruitList',data,{ noAuth : true });
}

/**
 * 最新求职匹配权重模板分页查询
 * 
*/
export function DateMatchNewJobApi(data)
{
  return request.get('DateMatch/NewJobList',data,{ noAuth : true });
}

/**
 * 最新求租匹配权重模板分页查询
 * 
*/
export function DateMatchNewInFoApi(data)
{
  return request.get('DateMatch/NewInFoList',data,{ noAuth : true });
}

/**
 * 动态模板列表
 */
export function getActiveTemplate(){
  return request.get(`DateMatch/List`,{},{ noAuth : true });
}
/**
 * @param {Object} url
 */
export function getActiveTemplatebak(url){
  return request.get(`DateMatch/${url}`,{},{ noAuth : true });
}

/**
 *  获取求组
 */
export function getDateM1(data){
  return request.get(`DateMatch/NearbyList`,data,{ noAuth : true });
}
/**
 *  获取出租
 */
export function getDateM2(data){	
  return request.get(`DateMatch/LetList`,data,{ noAuth : true });
}
/**
 *  获取转让、出售
 */
export function getDateM3(data){
  return request.get(`DateMatch/TransferSaleList`,data,{ noAuth : true });
}
/**
 *  获取求购
 */
export function getDateM4(data){
  return request.get(`DateMatch/BuyList`,data,{ noAuth : true });
}

/**
 *  通用匹配
 */
export function DateMatch(data){
  return request.post(`DateMatch/TotalDataMatching`,data,{ noAuth : true });
}
// 所有
export const AllListApi = (data) => {
	return request.get('Information/StatusAllList', data, { noAuth : true })
}