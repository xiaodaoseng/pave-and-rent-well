
import request from "@/utils/request.js";
/**
 * 悬赏接口
 * 
*/

/**
 * 悬赏新增
 * 
*/
export function RewardAddApi(data)
{
  return request.post("Reward/add",data,{ noAuth : true });
}

/**
 * 我的接取悬赏信息分页查询
 * 
*/
export function RewardOnApi(data)
{
  return request.get("RewardUser/OnList",data,{ noAuth : true });
}

/**
 * 我的接取悬赏信息分页查询
 * 
*/
export function RewardListApi(data)
{
  return request.get("Reward/list",data,{ noAuth : true });
}

/**
 * 悬赏信息分页查询
 * 
*/
export function RewardUserApi(data)
{
  return request.get("RewardUser/list",data,{ noAuth : true });
}

/**
 * 悬赏详情
 * 
*/
export function RewardDetailApi(id,data)
{
  return request.get(`Reward/get/list/${id}`,data,{ noAuth : true });
}
/**
 * 编辑悬赏  Reward/update
 */
export function RewardUpdateApi(data)
{
  return request.post(`Reward/update`,data,{ noAuth : true });
}
