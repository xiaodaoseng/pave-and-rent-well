// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------
import store from "../store";
import {
	getLocationApi
} from '@/api/api.js'
// 公共过滤器
export function filterEmpty(val) {
	let _result = '-'
	if (!val) {
		return _result
	}
	_result = val
	return _result
}
/**
 * 商户分类
 */
export function merCategoryFilter(status) {
	if (!status) {
		return ''
	}
	let arrayList = store.getters.merchantClassify;
	let array = arrayList.filter(item => status === item.id)
	if (array.length) {
		return array[0].name
	} else {
		return ''
	}
}

/**
 * 商铺类型
 */
export function merchantTypeFilter(status) {
	if (!status) {
		return ''
	}
	let arrayList = store.getters.merchantType;
	let array = arrayList.filter(item => status === item.id)
	if (array.length) {
		return array[0].name
	} else {
		return ''
	}
}

/**
 * 商户创建类型
 */
export function merCreateTypeFilter(status) {
	const statusMap = {
		'admin': '管理员创建',
		'apply': '商户入驻申请'
	}
	return statusMap[status]
}

/**
 * 商户类别
 */
export function selfTypeFilter(status) {
	const statusMap = {
		true: '自营',
		false: '非自营'
	}
	return statusMap[status]
}

/**
 * 日期去掉时间
 */
export function dateFormat(value) {
	if (!value) {
		return '';
	}
	return value.split(' ')[0];
}

/**
 * 退款状态
 */
export function refundStatusFilter(status) {
	const statusMap = {
		0: '待审核',
		1: '审核未通过',
		2: '退款中',
		3: '退款成功'
	}
	return statusMap[status]
}

/**
 * 订单状态
 */
export function orderStatusFilter(status) {
	const statusMap = {
		0: '待付款',
		1: '待发货',
		2: '部分发货',
		3: '待核销',
		4: '待收货',
		5: '已收货',
		6: '已完成',
		9: '已取消'
	}
	return statusMap[status]
}

/**
 * 支付方式
 */
export function payTypeFilter(status) {
	const statusMap = {
		'weixin': '微信',
		'alipay': '支付宝',
		'yue': '余额'
	}
	return statusMap[status]
}

/**
 * 获取当前经纬度
 */
export function getLocation() {
	return new Promise((resole, reject) => {
		uni.getLocation({
			type: 'wgs84',
			success: function(res) {
				resole(res)
			}
		})
	})

}

/**
 * 计算两点之间直线距离
 */
const algorithm = (point1, point2) => {
	let [x1, y1] = point1;
	let [x2, y2] = point2;
	let Lat1 = rad(x1); // 纬度
	let Lat2 = rad(x2);
	let a = Lat1 - Lat2; //	两点纬度之差
	let b = rad(y1) - rad(y2); //	经度之差
	let s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
		Math.cos(Lat1) * Math.cos(Lat2) * Math.pow(Math.sin(b / 2), 2)));
	//	计算两点距离的公式
	s = s * 6378137.0; //	弧长等于弧度乘地球半径（半径为米）
	s = Math.round(s * 10000) / 10000; //	精确距离的数值
	return s;

}
/**
 * 城市id
 */
export const getCityId = () => {
	var QQMapWX = require('../utils/qqmap-wx-jssdk');
	let qqmap = new QQMapWX({
		// key: 'SMJBZ-WCHK4-ZPZUA-DSIXI-XDDVQ-XWFX7' // 必填 //P7NBZ-MN4RG-4WUQT-QF6XU-STYG5-JUBDJ
		// key: '6R4BZ-WAB3W-JITRG-OE7GY-R2753-P3BZ2' // 必填
		key: 'AQYBZ-6SLCP-VH2DI-VTGDZ-CYAA6-M3B3E'
	})
	return new Promise((resolve, reject) => {
		uni.getLocation({
			type: 'gcj02',
			geocode: true, //必须要将geocode配置为true
			isHighAccuracy: true,
			altitude: true,
			success: function(res) {
				const {
					latitude,
					longitude
				} = res //res.latitude
				resolve(
					new Promise((so, je) => {
						//腾讯地图
						qqmap.reverseGeocoder({ //逆地址解析（经纬度 ==> 坐标位置）
							location: {
								latitude,
								longitude
							},
							success(mapRes) {
							
								let economize = (mapRes.result.ad_info.adcode)
									.substring(
										0, 2) + '0000';
								let adcode = (mapRes.result.ad_info.adcode)
									.substring(
										0, 4) + '00';
								let code = mapRes.result.ad_info.adcode;
								// so(Number(adcode + '00'))

								so({
									cityId: Number(adcode),
									cityName: mapRes.result
										.formatted_addresses.recommend,
									county: [economize, adcode, code],
									countyName: mapRes.result.ad_info
										.name.split(',')
								})
							},
							fail(e){
								
							}
						})
					})
				)

			},
			fail(e) {
				
				reject(e)
			}
		})
	})


}

const key = "6R4BZ-WAB3W-JITRG-OE7GY-R2753-P3BZ2"
export function getCityInfo(longitude, latitude) {
	var QQMapWX = require('../utils/qqmap-wx-jssdk');
	let qqmap = new QQMapWX({
		// key: 'SMJBZ-WCHK4-ZPZUA-DSIXI-XDDVQ-XWFX7' // 必填 //P7NBZ-MN4RG-4WUQT-QF6XU-STYG5-JUBDJ
		// key: '6R4BZ-WAB3W-JITRG-OE7GY-R2753-P3BZ2' // 必填
		key: key
	})
	return new Promise((so, je) => {
		//腾讯地图
		qqmap.reverseGeocoder({ //逆地址解析（经纬度 ==> 坐标位置）
			location: {
				latitude,
				longitude
			},
			success(mapRes) {
				let economize = (mapRes.result.ad_info.adcode).substring(0, 2) + '0000';
				let adcode = (mapRes.result.ad_info.adcode).substring(0, 4) + '00';
				let code = mapRes.result.ad_info.adcode;
				// so(Number(adcode + '00'))
				so({
					cityId: Number(adcode),
					cityName: mapRes.result.formatted_addresses.recommend,
					county: [economize, adcode, code],
					countyName: mapRes.result.ad_info.name.split(',')
				})
			}
		})
	})
}

export const getLocationII = (address) => {
	return getLocationApi({
		address,
		key:key
	})
}

//跳转页面
export const toPage = (url) => {
	uni.navigateTo({
		url
	})
}

//获取类目
export const getCategory = (category) => {
	let enums = {
		0: {
			name: '商铺',
			value: 883
		},
		1: {
			name: '写字楼',
			value: 884
		},
		2: {
			name: '公寓',
			value: 1014
		},
		3: {
			name: '厂房',
			value: 911
		},
		4: {
			name: '仓库',
			value: 885
		},
		5: {
			name: '土地',
			value: 912
		},
		6: {
			name: '房屋',
			value: 913
		},
	}

	return enums[category]
}

export const showLoading = () => {
	uni.showLoading({
		title: '加载中',
		mask: true
	})
}