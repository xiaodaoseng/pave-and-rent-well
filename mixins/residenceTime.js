let enterTime = '';
let outTime = '';
let stayTime = '';
import {
	addVisitRecord,
	getRewardDd
} from '@/api/user.js'
	import {
		mapGetters
	} from "vuex";
export default {
	data() {
		return {
			historyList: [{
					route: 'pages/details/shopRent/shopRent',
					type: 1
				}, {
					route: 'pages/details/unused/unused',
					type: 2
				},
				{
					route: 'pages/details/merchantService/merchantService',
					type: 3
				},
				{
					route: 'pages/details/supplyChain/supplyChain',
					type: 4
				},
				{
					route: 'pages/details/recruitWorkers/recruitWorkers',
					type: 5
				},
				{
					route: 'pages/details/askRent/askRent',
					type: 6,
					params: 'askrentId'
				},
				{
					route: 'pages/details/askRent/askRent',
					type: 7,
					params: 'buystoreId'
				},
				{
					route: 'pages/details/jobHunting/jobHunting',
					type: 10
				},
				{
					route: 'pages/details/project/project',
					type: 9
				},{
					route: 'pages/details/askSupply/askSupply',
					type : 11
				}
			]
		};
	},
	computed: {
		...mapGetters(['isLogin']),
	},
	watch: {
		"shareInfo.isInitStatus"(val) {
			if (val == 1) {
				// 用户进入时间
				this.$nextTick(() => {

					// if (this.shareInfo && this.shareInfo.shareId && this.$refs.countDown) {
					// 	this.$refs.countDown.startCountdown();
					// }
					if (this.$refs.countDown) {
						this.$refs.countDown.isMind = this.mind ;
						// if(this.mind == 1){
						// 	return;
						// }
						this.toGetReward((res) => {
							if(res.data.remand == 1){
								return;
							}
							enterTime = new Date();
							this.$refs.countDown.startCountdown(res.data);
						})
					}
				})
			}
		}
	},
	methods: {
		toGetReward(callback) {
			let id = this.$mp.page.options.id || this.$mp.page.options.askrentId || this.$mp.page.options.buystoreId;
			let shareInfo = this.$mp.page.$vm.shareInfo;
			let detailInfo = this.$mp.page.$vm.detailInfo;
			
			
			let curRoute = this.$mp.page.route;
			let stayRoute = this.historyList.filter(item => item.route == curRoute && (item.params ? (this.$mp.page
				.options[
					item.params]) : true));
					
			if(!this.isLogin){
				return;
			}
			getRewardDd({
				infoId: id,
				type: detailInfo.st == 1? 8 :stayRoute[0].type,
				status: detailInfo.st?detailInfo.st:'',
				storeCategory: detailInfo.storeCategory?detailInfo.storeCategory:'',
				sharerId: shareInfo ? shareInfo.shareId : ''
			}).then(res => {
				callback && callback(res)
			})
		}
	},
	onShow() {
		enterTime = new Date()
	},
	// onHide(){
	// 	// 用户退出时间
	// 	outTime = new Date();
	// 	//停留时间(毫秒)
	// 	stayTime = outTime.getTime() - enterTime.getTime();
	// 	let curRoute = this.$mp.page.route;
	// 	console.log('页面隐藏'+curRoute+':'+stayTime);
	// },
	onUnload() {
		if (this.$refs.countDown) {
			this.$refs.countDown.stopCountdown()
		}
		// 用户退出时间
		outTime = new Date();
		//停留时间(毫秒)
		stayTime = outTime.getTime() - enterTime.getTime();
		let curRoute = this.$mp.page.route;
		let id = this.$mp.page.options.id || this.$mp.page.options.askrentId || this.$mp.page.options.buystoreId;
		let bh = {
			1: '查看地址',
			2: '电话',
			3: '收藏',
			4: '查看图片'
		}
		let bhv = this.$mp.page.$vm.bhv;
		let detailInfo = this.$mp.page.$vm.detailInfo;
		
		
		let shareInfo = this.$mp.page.$vm.shareInfo;
		let stayRoute = this.historyList.filter(item => item.route == curRoute && (item.params ? (this.$mp.page.options[
			item.params]) : true));
		
		let behavior = [];
		bhv && bhv.forEach(item => {
			behavior.push(bh[item])
		})
		if (stayRoute.length > 0) {
			addVisitRecord({
				infoId: id,
				type: detailInfo&&detailInfo.st == 1? 8 :stayRoute[0].type,
				status:detailInfo&& detailInfo.st?detailInfo.st:'',
				storeCategory: detailInfo&&detailInfo.storeCategory?detailInfo.storeCategory:'',
				residence: stayTime / 1000,
				behavior: behavior.join("/"),
				sharerId: shareInfo ? shareInfo.shareId : ''
			})
		}
	},

};