import {
	DateMatch, //匹配通用
} from '@/api/template.js'
export default {
	data() {
		return {
			typesMatch: [{
					name: '全部',
					type: 'all'
				}, {
					name: '出租',
					type: 'jy',
					status: '0'
				},
				{
					name: '出售',
					type: 'jy',
					status: '1'
				},
				{
					name: '转让',
					type: 'jy',
					status: '2'
				}, {
					name: '求租',
					type: 'qz',
					status: '3'
				}, {
					name: '求购',
					type: 'qg',
					status: '4'
				},
				{
					name: '招工',
					type: 'zg',
					status: '6'
				}, {
					name: '求职',
					type: 'qzs',
					status: '5'
				}
			],
			//模板接口字段，用于筛选
			fileds: {
				'jy': {
					'0': {
						workingGroup: 'businessForm',
						avea: 'areaRange',
						supportingFacility: 'supp',
						rent: 'rentRange',
						storefrontType: 'pavementsType',
						storeCategory: 'storeCategory',
						floor: 'floors',
						district: 'dis',
						reutLongdate: 'leases',
						transferFee: 'sellAmount',

					},
					'1': {
						district: 'dis',
						avea: 'areaRange',
						floor: 'floors',
						sellOunt: 'serviceAmount',
						storeCategory: 'storeCategory',
						returnInvestment: 'userRate',
					},
					'2': {
						workingGroup: 'businessForm',
						avea: 'areaRange',
						supportingFacility: 'supp',
						rent: 'rentRange',
						storefrontType: 'pavementsType',
						floor: 'floors',
						district: 'dis',
						reutLongdate: 'leases',
						transferFee: 'sellAmount',
						storeCategory: 'storeCategory',
					}
				},
				'qz': {
					area: {
						type: 'range',
						skey: 'areaRange',
						ekey: 'areaRanges',
						sp: '-'
					},
					storeCategory: 'storeCategory',
					rent: {
						type: 'range',
						skey: 'rentRange',
						ekey: 'rentRanges',
						sp: '-'
					},
					categoryId: 'businessForm',
					floor: 'floors',
					sspre: 'supp',
					district: 'dis',
					rentYear: 'leases',
					storefrontType: 'pavementsType',
					transferFee: 'sellAmount',
				},
				'qg': {
					area: {
						type: 'range',
						skey: 'areaRange',
						ekey: 'areaRanges',
						sp: '-'
					},
					storeCategory: 'storeCategory',
					money: 'sellAmount',
					hopeArea: 'dis',
					floor: 'floors',
					rate: 'userRate',
				},
				'zg': {
					job: 'positions',
					industry: 'industry',
					experience: {
						type: 'range',
						skey: 'rangeExperience',
						ekey: 'rangeExperiences',
						sp: '-'
					},
					salary: {
						type: 'range',
						skey: 'salaryRange',
						ekey: 'salaryRanges',
						sp: '-'
					},
					jobMark: 'keywords'
				},
				'qzs': {
					job: 'positions',
					hopeIndustry: 'industry',
					workExperience: 'rangeExperience',
					salary: {
						type: 'range',
						skey: 'salaryRange',
						ekey: 'salaryRanges',
						sp: '-'
					},
					mark: 'keywords'
				}
			},
		};
	},
	methods: {
		async toMatch(data, page, sort, callback) {
			let emitData = this.filedSx(data);
			emitData.data.businessForm = emitData.data.businessForm && Array.isArray(emitData.data.businessForm) ?
				emitData.data.businessForm.join(',') : '';
			let res = null;
			let parmas = {
				page: page,
				screening: {
					city: data.city
					// city: this.navList[this.categoryOn].city
				},
				userMatch: {
					// sort: this.sxx[this.filterOn].sort,
					sort,
					appKey: 'xcx',
					...emitData.data,
					// ...this.navList[this.categoryOn],
					longitude: this.$location.longitude,
					latitude: this.$location.latitude
				}

			};


			res = await DateMatch(parmas);

			// 	res.data && res.data.list.forEach(item => {
			// 		if (item.askrentId) {
			// 			item.status = 3
			// 		} else if (item.buystoreId) {
			// 			item.status = 4;
			// 		}

			// 	})
			callback && callback(res);
		},
		//筛选字段
		filedSx(d) {

			let tag = {}
			let nf = {}
			let t = this.checkt('all', d)
			if (t == 'jy') {
				tag = this.typesMatch.filter(item => item.status == d.status)[0];
				let st = tag.status
				nf = this.fileds[t][st];
			} else {
				tag = this.typesMatch.filter(item => item.type == t)[0];
				nf = this.fileds[t];
			}
			return {
				// tag,
				data: this.filedGz(nf, d),
				// sd: d
			}
		},
		checkt(t, d) {
			if (t !== 'all') {
				return t;
			}
			if (d.askrentId) {
				t = 'qz';
			} else if (d.buystoreId) {
				t = 'qg';
			} else if (d.type == 5) {
				t = 'zg';
			} else if (d.type == 6) {
				t = 'qzs';
			} else {
				t = 'jy';
			}
			return t;
		},
		//字段规则
		filedGz(gz, data) {
			if (!data) return;
			let obj = {}

			for (let k in gz) {
				let gzk = gz[k];
				if (typeof gzk == "string") {

					obj[gzk] = data[k] ? data[k] : ' '
				} else {

					if (gzk.type == 'range') {
						obj[gzk.skey] = data[k] ? data[k].split(gzk.sp)[0] : ' '
						obj[gzk.ekey] = data[k] ? data[k].split(gzk.sp)[1] : ' '
					}
				}

			}
			obj.latitude = data.latitude;
			obj.longitude = data.longitude;
			obj.city = data.city;
			obj.district = data.district;
			obj.name = data.title;
			obj.infoId = data.id || data.askrentId || data.buystoreId;
			if (data.askrentId) {
				obj.type = 3;
			} else if (data.buystoreId) {
				obj.type = 4;
			} else {
				obj.type = data.status;
			}
			
			return obj;
		}
	},
	onShow() {

	},
	// onHide(){
	// 	// 用户退出时间
	// 	outTime = new Date();
	// 	//停留时间(毫秒)
	// 	stayTime = outTime.getTime() - enterTime.getTime();
	// 	let curRoute = this.$mp.page.route;
	// 	console.log('页面隐藏'+curRoute+':'+stayTime);
	// },
	onUnload() {

	}

};