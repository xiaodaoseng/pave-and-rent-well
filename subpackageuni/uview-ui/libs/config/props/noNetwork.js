/*
 * @Author       : LQ
 * @Description  :
 * @version      : 1.0
 * @Date         : 2021-08-20 16:44:21
 * @LastAuthor   : LQ
 * @lastTime     : 2021-08-20 17:16:39
 * @FilePath     : /u-view2.0/uview-ui/libs/config/props/noNetwork.js
 */
export default {
    // noNetwork
    noNetwork: {
        tips: '哎呀，网络信号丢失',
        zIndex: '',
        image: 'https://image.shanghepu.com/cdn/images/network.png'
	}

}
