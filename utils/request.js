// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2021 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import {
	HTTP_REQUEST_URL,
	HEADER,
	TOKENNAME,
	HEADERPARAMS
} from '@/config/app';
import {
	toLogin,
	checkLogin
} from '../libs/login';
import store from '../store';


/**
 * 发送请求
 */
function baseRequest(url, method, data, {
	noAuth = false,
	noVerify = false
}, params) {
	let Url = HTTP_REQUEST_URL,
		header = HEADER
	if (params != undefined) {
		header = HEADERPARAMS;
	}
	if (!noAuth) {
		//登录过期自动登录
		if (!store.state.app.token && !checkLogin()) {
			toLogin();
			return Promise.reject({
				msg: '未登录'
			});
		}
	}
	if (store.state.app.token) header[TOKENNAME] = store.state.app.token;
	return new Promise((reslove, reject) => {
		uni.request({
			url: Url + '/api/front/' + url,
			method: method || 'GET',
			header: header,
			data: data || {},
			success: (res) => {
				if (noVerify)
					reslove(res.data, res);
				else if (res.data.code == 200) {
					if (res.data.data&&res.data.data.list) {
						 let imagesUrl = uni.getStorageSync("$noImageShow")  
						 let f = uni.getStorageSync("$noImageFileds")  
						res.data.data.list.forEach(item => {
							let find = f.findIndex(fit => item.hasOwnProperty(fit));
							if(find>-1&&!item[f[find]]){
								item[f[find]]=imagesUrl
							}
							if(find<0 && item['managementResponse']){
								let itFind = f.findIndex(fit => item['managementResponse'].hasOwnProperty(fit));
								
								if(itFind>-1&& !item['managementResponse'][f[itFind]]){
									item['managementResponse'][f[itFind]]=imagesUrl
								}
							}
							
							if(item.list){
								item.list.forEach(listIt=>{
									let liFind = f.findIndex(fit => listIt.hasOwnProperty(fit));
									if(liFind>-1&&!listIt[f[liFind]]){
										listIt[f[liFind]]=imagesUrl
									}
								})
							}
						})
					}
					reslove(res.data, res);
				} else if ([410000, 410001, 410002, 401].indexOf(res.data.code) !== -1) {
					toLogin();
					reject(res.data);
				} else if (res.data.code == 500) {
					if (res.data.message.indexOf('登录信息已过期') != -1) {
						store.commit("LOGOUT");
					}
					reject(res.data.message || '系统异常');
				} else if (res.data.code == 400) {
					reject(res.data.message || '参数校验失败');
				} else if (res.data.code == 404) {
					reject(res.data.message || '没有找到相关数据');
				} else if (res.data.code == 403) {
					reject(res.data.message || '没有相关权限');
				} else
					reject(res.data.message || '系统错误');
			},
			fail: (msg) => {
				reject('请求失败');
			}
		})
	});
}

const request = {};

['options', 'get', 'post', 'put', 'head', 'delete', 'trace', 'connect'].forEach((method) => {
	request[method] = (api, data, opt, params) => baseRequest(api, method, data, opt || {}, params)
});



export default request;