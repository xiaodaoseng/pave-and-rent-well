// +---------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +---------------------------------------------------------------------
// | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
// +---------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +---------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +---------------------------------------------------------------------

import Vue from 'vue'
import App from './App'
import store from './store'
import Cache from './utils/cache'
import util from 'utils/util'
// import i18n from './i18n/index.js' // 国际化
import configs from './config/app.js'
import * as Order from './libs/order';
import * as filters from '@/filters';
import slideNav from '@/components/slide-nav/index.vue'
import {
	getApplet
} from '@/api/api.js'
import {
	dealParams
} from '@/api/dealParams.js'
import residenceTime from '@/mixins/residenceTime.js'
import {
	getCityId,
	getLocation
} from '@/filters/commFilter.js'

let curCity = ""

//根据定位逻辑
// 如果之前选中  用之前
const storageCity = uni.getStorageSync('city');
const storageCurData = uni.getStorageSync('curCityData');


// 如果之前没选中  默认是使用后台设定的默认城市


function reLocation(callback) {
	getCityId().then(city => {
		
		if (!city) {
			reLocation(callback);
			return;
		}
		
		Vue.prototype.$address = city.cityName
		Vue.prototype.$cityCode = city.county
		Vue.prototype.$cityName = city.countyName
		if(storageCity && storageCurData){
			Vue.prototype.$city = storageCity;
			Vue.prototype.$curCityData = JSON.parse(storageCurData);
			
		}else{
			Vue.prototype.$city = city.cityId;
			Vue.prototype.$curCityData = {
				regionId: city.county[1],
				regionName: city.countyName[2]
			}
		}
		
		app.$root.curCity = city.cityId;
		getLocation().then(location => {
			getApplet({
				...location,
				province: city.county[0],
				city: city.county[1],
				district: city.county[2]
			})

		})
		initLocation();
		callback && callback();
	});

}
Vue.prototype.$splitImage = (imgVid) => {
		const videoArray = [];
		const imageArray = [];
		let t = imgVid ? imgVid.split(',') : '';
		if (!t) {
			t = [noImageShow]
		}
		const videoExtensions = /\.(mp4|avi|mov)$/i;
		const imageExtensions = /\.(jpg|jpeg|png|gif)$/i;
		t.forEach(item => {
			const extension = item.match(/\.[0-9a-z]+$/i) ? item.match(/\.[0-9a-z]+$/i)[0] : '';

			if (extension.match(videoExtensions)) {
				videoArray.push(item);
			} else if (extension.match(imageExtensions)) {
				imageArray.push(item);
			}
		});
		if (imageArray.length < 1) {
			imageArray.push(noImageShow);
		}
		return {
			videoArray,
			imageArray
		};
	},
	// reLocation()
	Vue.prototype.$setCity = function(cid) {
		Vue.prototype.$city = cid;
		uni.setStorageSync('city', cid);
	}
Vue.prototype.$setCurCityData = function(data) {
	Vue.prototype.$curCityData = data;
	uni.setStorageSync('curCityData', JSON.stringify(data));
}

function initLocation() {
	getLocation().then(location => {
		const {
			latitude,
			longitude,
		} = location;
		Vue.prototype.$location = {
			latitude,
			longitude
		}
		Vue.prototype.$chooseLocation = {
			latitude,
			longitude
		}
		getApplet(location)

	})
}

// initLocation();
Vue.prototype.$setChooseLocation = function(location) {
	Vue.prototype.$chooseLocation = {
		...location
	}
	console.log('set', location)
}
let noImageShow = "https://image.shanghepu.com/cdn/images/noImg.png"
let noImageFileds = ['shopfrontPhoto', 'thumbnail']
Vue.prototype.$noImageShow = noImageShow
Vue.prototype.$noImageFileds = noImageFileds
uni.setStorageSync("$noImageShow", noImageShow)
uni.setStorageSync("$noImageFileds", noImageFileds)
Vue.prototype.$reLocation = reLocation;
Vue.prototype.$bus = new Vue()
Vue.prototype.dealParams = dealParams;


// setTimeout(()=>{
// 	getLocation()
// },5000)
Vue.mixin(residenceTime)
Vue.mixin({
	methods: {
		setData: function(obj, callback) {
			let that = this;
			let keys = [];
			let val, data;
			Object.keys(obj).forEach(function(key) {
				keys = key.split('.');
				val = obj[key];
				data = that.$data;
				keys.forEach(function(key2, index) {
					if (index + 1 == keys.length) {
						that.$set(data, key2, val);
					} else {
						if (!data[key2]) {
							that.$set(data, key2, {});
						}
					}
					data = data[key2];
				})
			});
			callback && callback();
		}
	},
	// #ifdef MP
	//发送给朋友
	onShareAppMessage(res) {
		// 此处的distSource为分享者的部分信息，需要传递给其他人
		let that = this;
		let u = this.shareInfo.sharePath ? this.shareInfo.sharePath : '/' + that.$mp.page.route;
		return {
			title: this.shareInfo.title,
			path: "/pages/index/index?path=" + u + "&infoId=" + this.shareInfo.infoId + "&spread=" + (this
					.$store.state.app.uid || 0) + '&spreadType=' + (this.shareInfo.infoType || this.shareInfo
					.infoType === 0 ? this.shareInfo.infoType : '') + '&storeCategory=' + this.detailInfo
				.storeCategory + '&st=' + this.detailInfo.st,
			imageUrl: this.shareInfo.img,
		};
	},
	//分享到朋友圈
	onShareTimeline() {

		let that = this;
		let u = this.shareInfo.sharePath ? this.shareInfo.sharePath : '/' + this.$mp.page.route;
	
		return {
			title: this.shareInfo.title,
			path: "/pages/index/index?path=" + u + "&infoId=" + this.shareInfo.infoId + "&spread=" + (this
				.$store.state.app.uid || 0) + '&spreadType=' + (this.shareInfo.infoType || this.shareInfo
				.infoType === 0 ? this.shareInfo.infoType : ''),
			query: {

				infoId: this.shareInfo.infoId,
				spreadType: this.shareInfo.infoType || this.shareInfo.infoType === 0 ? this.shareInfo.infoType :
					'',
				spread: this.$store.state.app.uid || 0
			},
			imageUrl: this.shareInfo.img ? this.shareInfo.img : noImageShow,
		};
	},
	// #endif
});

App.mpType = 'app';

Vue.prototype.$util = util;
Vue.prototype.$config = configs;
Vue.prototype.$Cache = Cache;
Vue.prototype.$store = store;
Vue.prototype.$eventHub = new Vue();
Vue.config.productionTip = false
Vue.prototype.$Order = Order;
Vue.component('slideNav', slideNav)
Object.keys(filters).forEach(key => {
	Vue.filter(key, filters[key])
})

import uView from '@/subpackageuni/uview-ui/index.js'
Vue.use(uView)
// #ifdef H5
import {
	parseQuery
} from "./utils";
import Auth from './libs/wechat';
import {
	SPREAD
} from './config/cache';
Vue.prototype.$wechat = Auth;

Vue.prototype.$imagesCdn = 'https://image.shanghepu.com/cdn/'

let cookieName = "VCONSOLE",
	query = parseQuery(),
	urlSpread = query["spread"],
	vconsole = query[cookieName.toLowerCase()],
	md5Crmeb = "b14d1e9baeced9bb7525ab19ee35f2d2", //CRMEB MD5 加密开启vconsole模式
	md5UnCrmeb = "3dca2162c4e101b7656793a1af20295c"; //UN_CREMB MD5 加密关闭vconsole模式
if (urlSpread) {
	urlSpread = parseInt(urlSpread);
	Cache.setItem({
		name: 'spread',
		value: urlSpread,
	})
}
if (vconsole !== undefined) {
	if (vconsole === md5UnCrmeb && Cache.has(cookieName))
		Cache.clear(cookieName);
} else vconsole = Cache.get(cookieName);

import VConsole from './components/vconsole.min.js'

if (vconsole !== undefined && vconsole === md5Crmeb) {
	Cache.set(cookieName, md5Crmeb, 3600);
	let vConsole = new VConsole();
}
// #endif

App.mpType = 'app'


const app = new Vue({
	...App,
	store,
	// i18n,
	Cache,
	data: {
		curCity
	}
})
app.$mount();