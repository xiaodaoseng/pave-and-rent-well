// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import {
	getMerCollectAddApi,
	getMerCollectCancelApi
} from '@/api/merchant.js';
/**
 * 关注、取消关注商户
 */
export function followMer(follow, id) {
	return new Promise((resolve, reject) => {
		if (follow) {
			getMerCollectCancelApi(id).then(res => {
				resolve(res);
			}).catch(err => {
				this.$util.Tips({
					title: err
				});
			});
		} else {
			getMerCollectAddApi(id).then(res => {
				resolve(res);
			}).catch(err => {
				this.$util.Tips({
					title: err
				});
			});
		}
	});
}
